<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JenisCetak extends Model
{
    protected $table = 'jenis_cetak';

    protected $fillable = [
        'nama_cetak',
        'harga'               
    ];


    public static function getDefaultValues()
    {
        return (object) [
            'nama_cetak' => '',
            'harga' => ''           
        ];
    }
}
