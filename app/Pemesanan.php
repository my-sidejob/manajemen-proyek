<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pemesanan extends Model
{
    protected $table = 'pemesanan';

    protected $fillable = [
        'kode',
        'tanggal',
        'design',
        'jenis_cetak_id',
        'total',
        'status_pemesanan',
        'status_pembayaran',
        'konsumen_id',
        'user_id'
    ];

    private $statusPemesanan = [
        '1' => 'Menunggu Konfirmasi',
        '2' => 'Pesanan Telah Dikonfirmasi',
        '3' => 'Pesanan Sedang Diproses',
        '4' => 'Pesanan Selesai',
        '5' => 'Pesanan Telah Diambil',
        '6' => 'Pesanan Dibatalkan'
    ];

    private $statusPembayaran = [
        '1' => 'Belum Dibayar',
        '2' => 'Pembayaran DP Diterima',
        '3' => 'Pembayaran Lunas'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
    
    public function konsumen()
    {
        return $this->belongsTo('App\Konsumen');
    }

    public function cetak()
    {
        return $this->belongsTo('App\JenisCetak', 'jenis_cetak_id', 'id');
    }

    public function pengeluaran()
    {
        return $this->hasMany('App\Pengeluaran');
    }
    
    public function details()
    {
        return $this->hasMany('App\DetailPemesanan');
    }

    public static function getDefaultValues()
    {
        return (object) [
            'kode' => '',
            'tanggal' => date('Y-m-d'),
            'design' => '',
            'jenis_cetak_id' => '',
            'total' => '',
            'status_pemesanan' => '',
            'konsumen_id' => '',
            'user_id' => ''
        ];
    }

    public function pembayaran()
    {
        return $this->hasMany('App\Pembayaran');
    }

    public function textStatusPemesanan() {
        
        return $this->statusPemesanan[$this->status_pemesanan];
    }

    public function textStatusPembayaran() {
        
        return $this->statusPembayaran[$this->status_pembayaran];
    }
}
