<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pembayaran extends Model
{
    protected $fillable = [
        'tanggal_pembayaran',
        'nominal',
        'pemesanan_id',
        'bukti_pembayaran'
    ];

    protected $table = 'pembayaran';

    public function pemesanan()
    {
        return $this->belongsTo('App\Pemesanan');
    }
}
