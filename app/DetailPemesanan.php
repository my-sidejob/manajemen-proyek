<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailPemesanan extends Model
{
    protected $table = 'detail_pemesanan';

    protected $fillable = [
        'produk_id',
        'jumlah',        
        'pemesanan_id'
    ];

    public function produk()
    {
        return $this->belongsTo('App\Produk');
    }
}
