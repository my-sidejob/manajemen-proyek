<?php

namespace App\Http\Requests;

use App\User;
use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = User::find($this->user);
        $rules = [
            'nama' => ['required'],
            'jabatan' => ['required'],
        ];
        if($this->method() == 'POST'){
            $rules += [
                'email' => ['required', 'string', 'regex:"[@]"', 'unique:users'],
                'username' => ['required', 'unique:users'],
                'password' => ['required'],
            ];
        } else {
            $rules += [
                'email' => ['required', 'string', 'regex:"[@]"', 'unique:users,email,'.$user->id],
                'username' => ['required', 'unique:users,username,'.$user->id],                
            ];
        }

        return $rules;
    }
}
