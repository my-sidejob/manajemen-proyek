<?php

namespace App\Http\Middleware;

use Closure;

class OwnerOnly
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->user()->jabatan == 1){
            return $next($request);
        } else {
            return redirect()->route('dashboard')->with('warning', 'Anda tidak bisa mengakses halaman tersebut');
        }
    }
}
