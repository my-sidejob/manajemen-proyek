<?php

namespace App\Http\Controllers;

use App\Pemesanan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    public function index()
    {
        $data['total_pemesanan'] = Pemesanan::all()->count('*');
        $data['sedang_dikerjakan'] = Pemesanan::whereIn('status_pemesanan', [1,2,3])->count();
        $data['pesanan_selesai'] = Pemesanan::whereIn('status_pemesanan', [4,5])->count();
        $data['pesanan_masuk'] = Pemesanan::where('status_pemesanan', 1)->count();
        $data['pemesanan'] = Pemesanan::where('status_pemesanan', 1)->get();

        /* grafik penjualan bulanan
        SELECT count(*) as jumlah, MONTH(tanggal) as bulan, YEAR(tanggal) as tahun FROM `pemesanan` 
        group by MONTH(tanggal) order by YEAR(tanggal), MONTH(tanggal)
        */ 

        $penjualan = DB::table('pemesanan')->selectRaw("count(*) as jumlah, MONTH(tanggal) as bulan, YEAR(tanggal) as tahun")
                                ->whereIn('status_pemesanan', [4,5])
                                ->groupByRaw('MONTH(tanggal), YEAR(tanggal)')
                                ->orderByRaw('YEAR(tanggal), MONTH(tanggal)')
                                ->get();
        //dd($penjualan);
        $tanggal = [];
        foreach($penjualan as $tgl) {
            array_push($tanggal, $tgl->tahun .'-'.$tgl->bulan);
        }

        $jumlah = [];

        foreach($penjualan as $jml) {
            array_push($jumlah, $jml->jumlah);
        }

        $data['tanggal'] = $tanggal;
        $data['jumlah'] = $jumlah;

        return view('dashboard', $data);
    }
}
