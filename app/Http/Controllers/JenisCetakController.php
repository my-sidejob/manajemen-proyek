<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\JenisCetak;

class JenisCetakController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $jenis_cetak = JenisCetak::orderBy('nama_cetak', 'asc')->get();
        return view('jenis_cetak.index', compact('jenis_cetak'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $jenis_cetak = JenisCetak::getDefaultValues();
        return view('jenis_cetak.form', compact('jenis_cetak'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama_cetak' => 'required',
            'harga' => 'required'
        ]);
        JenisCetak::create($request->all());
        return redirect()->route('jenis_cetak.index')->with('success', 'Berhasil menambah data jenis cetak');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $jenis_cetak = JenisCetak::find($id);
        return view('jenis_cetak.form', compact('jenis_cetak'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama_cetak' => 'required',
            'harga' => 'required'
        ]);
        
        $jenis_cetak = JenisCetak::find($id);

        $jenis_cetak->update($request->all());

        return redirect()->route('jenis_cetak.index')->with('success', 'Berhasil mengubah data jenis cetak');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $jenis_cetak = JenisCetak::find($id);

        $jenis_cetak->delete();

        return redirect()->route('jenis_cetak.index')->with('success', 'Berhasil menghapus data jenis cetak');
    }
}
