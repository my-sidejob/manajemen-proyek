<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class ChangePasswordController extends Controller
{
    public function index()
    {
        return view('ubah_password');
    }

    public function change(Request $request)
    {
        $this->validate($request, [
            'old_password' => 'required|min:6|max:24',
            'password' => 'required|max:24|min:6|confirmed',
        ]);
        if (!(Hash::check($request->old_password, Auth::user()->password))) {
            // The passwords matches
            return redirect()->back()->with("failed_password","Password lama tidak cocok!");
        }
        if(strcmp($request->old_password, $request->password) == 0){
            return redirect()->back()->with("failed_password","Password baru dan lama tidak boleh sama!");
        }

        $user = Auth::user();
  
        $user->password = $request->password;
        $user->save();


        return redirect()->back()->with("success","Berhasil mengubah password anda.");
    }
}
