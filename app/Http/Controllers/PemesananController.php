<?php

namespace App\Http\Controllers;

use App\JenisCetak;
use App\Konsumen;
use App\Pemesanan;
use App\Produk;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PemesananController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pemesanan = Pemesanan::getDefaultValues();
        $konsumen = Konsumen::orderBy('nama_konsumen', 'asc')->get();
        $products = Produk::orderBy('nama_produk', 'asc')->get();
        $jenis_cetak = JenisCetak::all();
        return view('pemesanan.form', compact('pemesanan', 'konsumen', 'products', 'jenis_cetak'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function showAll()
    {
        $pemesanan = Pemesanan::orderBy('status_pembayaran', 'asc')->orderBy('status_pemesanan', 'asc')->orderBy('tanggal', 'desc')->get();

        return view('pemesanan.index', compact('pemesanan'));
    }

    public function showPemesanan($status_pemesanan)
    {
        if($status_pemesanan == 3){
            $pemesanan = Pemesanan::whereIn('status_pemesanan', ['2', '3'])
                                ->where('status_pembayaran', '>', 1)    
                                ->orderBy('status_pembayaran', 'asc')->orderBy('status_pemesanan', 'asc')->orderBy('tanggal', 'desc')->get();    
        } else if($status_pemesanan == 2){
            $pemesanan = Pemesanan::whereIn('status_pemesanan', ['2'])
                    ->where('status_pembayaran', 1)    
                    ->orderBy('status_pembayaran', 'asc')->orderBy('status_pemesanan', 'asc')->orderBy('tanggal', 'desc')->get(); 
        } else {
            $pemesanan = Pemesanan::where('status_pemesanan', $status_pemesanan)->orderBy('status_pembayaran', 'asc')->orderBy('status_pemesanan', 'asc')->orderBy('tanggal', 'desc')->get();
        }
        $number = $status_pemesanan;
        return view('pemesanan.index', compact('pemesanan', 'number'));
    }


    public function changeStatus(Request $request, $pemesanan_id, $status)
    {

        $pemesanan = Pemesanan::find($pemesanan_id);

        //pengambilan barang 
        if($status == 5){
            $paid = $pemesanan->pembayaran->sum('nominal');
            if($pemesanan->total != $paid){
                return redirect()->back()->with('error', 'Gagal mengambil pesanan karena konsumen belum membayar lunas.');
            } else {
                $status = 5;
            }
        }

        $pemesanan->update([
            'status_pemesanan' => $status
        ]);

        if($status == 2){
            $text = 'Berhasil mengkonfirmasi pemesanan '. $pemesanan->kode;
        } 

        if($status == 3){
            $text = 'Pemesanan '. $pemesanan->kode . ' sedang di proses';
        }
        
        if($status == 4){
            $text = 'Proses Pemesanan '. $pemesanan->kode . ' telah selesai';
        }

        if($status == 5){
            $text = 'Pemesanan '. $pemesanan->kode . ' berhasil diambil oleh konsumen';
        }

        

        return redirect()->back()->with('success', $text);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pemesanan = Pemesanan::find($id);

        return view('pemesanan.show', compact('pemesanan'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'tanggal' => 'required',
            'konsumen_id' => 'required',
            'jenis_cetak_id' => 'required',
            'produk_id' => 'required',
            'qty' => 'required'
        ]);

        if($request->hasFile('design')){
            $format = $request->design->extension();
            $rename = 'design_'.strtotime('now').'_'.$request->konsumen_id.'.'.$format;

            $request->design->storeAs('design', $rename);
        } else {
            $rename = null;
        }

        $nama_konsumen = Konsumen::find($request['konsumen_id'])->nama_konsumen;

        $jenis_cetak = JenisCetak::find($request['jenis_cetak_id']);

        $data = [
            'tanggal' => $request['tanggal'],
            'konsumen_id' => $request['konsumen_id'],
            'nama_konsumen' => $nama_konsumen,
            'kode' => 'INV-'.$request['konsumen_id'].'-'.strtotime("now"),
            'design' => $rename,
            'jenis_cetak_id' => $request['jenis_cetak_id'],
            'nama_cetak' => $jenis_cetak->nama_cetak,
            'harga_cetak' => $jenis_cetak->harga,
            'qty' => $request['qty'],
            'produk_id' => $request['produk_id'], 
            'user' => $request->user()->id,       
        ];

        //dd($data);
        
        return view('pemesanan.proses', $data);
        //dd($request->toArray());
    }


    public function process(Request $request)
    {
        $pemesanan = Pemesanan::create([
            "kode" => $request->kode,
            "tanggal" => $request->tanggal,
            "design" => $request->design,
            "jenis_cetak_id" => $request->jenis_cetak_id,
            "total" => $request->total,
            "status_pemesanan" => "1",
            "konsumen_id" => $request->konsumen_id,
            "user_id" => $request->user()->id,
        ])->id;

        for ($i=0; $i < count($request->produk_id); $i++) { 
            DB::table('detail_pemesanan')->insert([
                'produk_id' => $request->produk_id[$i],
                'jumlah' => $request->qty[$i],
                'pemesanan_id' => $pemesanan,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);
        }

        return redirect()->route('pemesanan.index')->with('success', 'Pemesanan berhasil dibuat!');
    }

    public function cancel($pemesanan_id)
    {
        $pemesanan = Pemesanan::find($pemesanan_id);

        if($pemesanan->exists == true){
            $pemesanan->update([
                'status_pemesanan' => 6
            ]);
        }

        return redirect()->back()->with('success', 'berhasil membatalkan pesanan');
    }
        

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
