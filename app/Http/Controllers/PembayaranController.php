<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pemesanan;
use App\Pembayaran;
use Carbon\Carbon;

class PembayaranController extends Controller
{
    public function index($pemesanan_id)
    {
        $data['pemesanan'] = Pemesanan::find($pemesanan_id);
        $pembayaran = Pembayaran::where('pemesanan_id', $pemesanan_id);
        $data['sisa'] = $data['pemesanan']->total - $pembayaran->sum('nominal');
        $data['pembayaran'] = $pembayaran->get();
        return view('pembayaran.modal', $data);
    }

    public function store(Request $request, $pemesanan_id)
    {
        $format = $request->bukti_pembayaran->extension();
        $rename = 'bukti_pembayaran_'.strtotime('now').'_'.$request->pemesanan_id.'.'.$format;

        $request->bukti_pembayaran->storeAs('bukti_pembayaran', $rename);

        $pemesanan = Pemesanan::find($pemesanan_id);
        $pembayaran = Pembayaran::where('pemesanan_id', $pemesanan_id);

        $fifty = ($pemesanan->total * 50) / 100;
        
        $paid = $pembayaran->sum('nominal');

        $sisa = $pemesanan->total - $paid;

        if($request->nominal > $pemesanan->total || $request->nominal > $sisa){
            return redirect()->back()->with('error', 'Gagal melakukan pembayaran pada pesanan '. $pemesanan->kode .' karena jumlah pembayaran melebihi total / sisa pemesanan. ');
        }
        
        if($paid == 0) {
            //langsung lunas
            if($request->nominal == $pemesanan->total){

                $pembayaran->create([
                    'nominal' => $request->nominal,
                    'tanggal_pembayaran' => Carbon::now(),
                    'pemesanan_id' => $pemesanan_id
                ]);

                $pemesanan->update([
                    'status_pembayaran' => '3'
                ]);

                return redirect()->back()->with('success', 'Berhasil melakukan pembayaran Lunas pada pesanan '. $pemesanan->kode);

            } else {
                //DP dulu
                if($fifty > $request->nominal){
                    return redirect()->back()->with('error', 'Gagal melakukan pembayaran pada pesanan '.$pemesanan->kode.', minimal DP 50%.');
                } else {
                    $pembayaran->create([
                        'nominal' => $request->nominal,
                        'tanggal_pembayaran' => Carbon::now(),
                        'pemesanan_id' => $pemesanan_id,
                        'bukti_pembayaran' => $rename
                    ]);
    
                    $pemesanan->update([
                        'status_pembayaran' => '2'
                    ]);
    
                    return redirect()->back()->with('success', 'Berhasil melakukan pembayaran DP pada pesanan '. $pemesanan->kode);
                }
            }
        } else {

            //angsuran ke 2 (pelunasan)
           

            if($request->nominal != $sisa){
                return redirect()->back()->with('error', 'Gagal melakukan pelunasan pada pesanan '.$pemesanan->kode.', mohon untuk membayar sisanya sebesar Rp. '.number_format($sisa));
            } else {
                $pembayaran->create([
                    'nominal' => $request->nominal,
                    'tanggal_pembayaran' => Carbon::now(),
                    'pemesanan_id' => $pemesanan_id,
                    'bukti_pembayaran' => $rename
                ]);

                $pemesanan->update([
                    'status_pembayaran' => '3'
                ]);

                return redirect()->back()->with('success', 'Berhasil melakukan pelunasan Pembayaran pada pesanan '. $pemesanan->kode);
            }
        }
    }
}
