<?php

namespace App\Http\Controllers;

use App\Pemesanan;
use App\Pengeluaran;
use Illuminate\Http\Request;

class LaporanController extends Controller
{
    public function penjualan(Request $request)
    {
        $type = 'penjualan';

        if(isset($_GET['search'])){
            
            $start_date = $request->start;
            $end_date = $request->end;

            $data = Pemesanan::whereBetween('tanggal', [$start_date, $end_date])
                ->whereIn('status_pemesanan', [4, 5])
                ->where('status_pembayaran', 3)
                ->orderBy('tanggal', 'asc')->get();

        } else {
            $start_date = '';
            $end_date = '';
            $data = null;
        }

        return view('laporan.index', compact('data', 'start_date', 'end_date', 'type'));
    }

    public function pengeluaran(Request $request)
    {
        $type = 'pengeluaran';

        if(isset($_GET['search'])){
            $start_date = $request->start;
            $end_date = $request->end;

            $data = Pengeluaran::whereBetween('tanggal', [$start_date, $end_date])
                ->orderBy('tanggal', 'asc')->get();

        } else {
            $start_date = '';
            $end_date = '';
            $data = null;
        }

        return view('laporan.index', compact('data', 'start_date', 'end_date', 'type'));
    }

    public function labaRugi(Request $request)
    {
        $type = 'laba rugi';

        if(isset($_GET['search'])){
            $start_date = $request->start;
            $end_date = $request->end;

            $data['pengeluaran'] = Pengeluaran::whereBetween('tanggal', [$start_date, $end_date])
                ->orderBy('tanggal', 'asc')->sum('nominal_pengeluaran');
            $data['pemasukan'] = Pemesanan::whereBetween('tanggal', [$start_date, $end_date])
                                ->whereIn('status_pemesanan', [4, 5])
                                ->where('status_pembayaran', 3)
                                ->orderBy('tanggal', 'asc')->sum('total');
            $data['laba_rugi'] = $data['pemasukan'] - $data['pengeluaran'];
            //$data['seluruh'] = $data['pemasukan'] + $data['pengeluaran'];
            if($data['laba_rugi'] != 0 && $data['pemasukan'] != 0){
                $data['persentase'] = round(($data['laba_rugi']/$data['pemasukan'])  * 100);         
            } else {
                $data['persentase'] = 100;
            }

        } else {
            $start_date = '';
            $end_date = '';
            $data = null;
        }

        return view('laporan.index', compact('data', 'start_date', 'end_date', 'type'));
        
    }
}
