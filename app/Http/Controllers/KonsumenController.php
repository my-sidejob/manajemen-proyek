<?php

namespace App\Http\Controllers;

use App\Http\Requests\KonsumenRequest;
use Illuminate\Http\Request;
use App\Konsumen;

class KonsumenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $konsumen = Konsumen::orderBy('nama_konsumen', 'asc')->get();
        return view('konsumen.index', compact('konsumen'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $konsumen = Konsumen::getDefaultValues();
        return view('konsumen.form', compact('konsumen'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(KonsumenRequest $request)
    {
        Konsumen::create($request->all());
        return redirect()->route('konsumen.index')->with('success', 'Berhasil menambah data konsumen');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $konsumen = Konsumen::find($id);
        return view('konsumen.form', compact('konsumen'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(KonsumenRequest $request, $id)
    {
        $konsumen = Konsumen::find($id);

        $konsumen->update($request->all());

        return redirect()->route('konsumen.index')->with('success', 'Berhasil mengubah data konsumen');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $konsumen = Konsumen::find($id);

        $konsumen->delete();

        return redirect()->route('konsumen.index')->with('success', 'Berhasil menghapus data konsumen');
    }
}
