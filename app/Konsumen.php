<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Konsumen extends Model
{
    protected $table = 'konsumen';

    protected $fillable = [
        'nama_konsumen',
        'email',
        'instansi',
        'alamat',
        'no_telp',        
    ];


    public static function getDefaultValues()
    {
        return (object) [
            'nama_konsumen' => '',
            'email' => '',
            'instansi' => '',
            'alamat' => '',
            'no_telp' => '',
        ];
    }
}
