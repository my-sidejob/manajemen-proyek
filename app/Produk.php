<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Produk extends Model
{
    protected $table = 'produk';

    protected $fillable = [
        'nama_produk',
        'bahan_baku',
        'keterangan',
        'harga_satuan',
        'kategori'
    ];

    public static function getDefaultValues()
    {
        return (object) [
            'nama_produk' => '',
            'bahan_baku' => '',
            'keterangan' => '',
            'harga_satuan' => '',
            'kategori' => ''
        ];
    }
}
