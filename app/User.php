<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nama', 'username', 'email', 'password', 'jabatan'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function setPasswordAttribute($val)
    {
        return $this->attributes['password'] = bcrypt($val);
    }


    public static function getDefaultValues()
    {
        return (object) [
            'nama' => '', 
            'username' => '', 
            'email' => '', 
            'password' => '', 
            'jabatan' => ''
        ];
    }

    public function getJabatan()
    {
        if($this->jabatan == 1){
            return 'Owner';
        } else if($this->jabatan == 2){
            return 'Finance';
        } else if($this->jabatan == 3){
            return 'CSA / Marketing';
        } else if ($this->jabatan == 4){
            return 'Produksi';
        }
    }
}
