<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pengeluaran extends Model
{
    protected $table = 'pengeluaran';

    protected $fillable = [
        'tanggal',
        'nama_pengeluaran',
        'nominal_pengeluaran',
        'keterangan',
        'pemesanan_id',
        'user_id'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function pemesanan()
    {
        return $this->belongsTo('App\Pemesanan');
    }

    public static function getDefaultValues()
    {
        return (object) [
            'tanggal' => date('Y-m-d'),
            'nama_pengeluaran' => '',
            'nominal_pengeluaran' => '',
            'keterangan' => '',
            'pemesanan_id' => '',
            'user_id' => ''
        ];
    }
}
