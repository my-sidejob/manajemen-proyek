<div class="modal-header">
    <h5 class="modal-title">Form Pengeluaran</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
    <div class="row text-left">
        <div class="col-md-12">
            <form action="{{ (!isset($pengeluaran->id)) ?  route('pengeluaran.store') : route('pengeluaran.update', $pengeluaran->id) }}" method="post">
                @csrf

                @if(isset($pengeluaran->id))
                    @method('put')
                @endif

                <div class="form-group row">
                    <input type="hidden" name="pemesanan_id" value="{{ $pemesanan_id }}">
                    <div class="col-sm-12 mb-2">
                        <label>Tanggal</label>
                        <input type="date" class="form-control form-control-user" name="tanggal" placeholder="Tanggal" value="{{ old('tanggal', $pengeluaran->tanggal) }}">
                        @error('tanggal')
                            <span class="invalid-feedback d-block" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-12 mb-2">
                        <label>Nama Pengeluaran</label>
                        <input type="text" class="form-control form-control-user" name="nama_pengeluaran"  placeholder="Masukan Nama Pengeluaran" value="{{ old('nama_pengeluaran', $pengeluaran->nama_pengeluaran) }}">
                        @error('nama_pengeluaran')
                            <span class="invalid-feedback d-block" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-sm-12 mb-2">
                        <label>Nominal Pengeluaran</label>
                        <input type="number" class="form-control form-control-user" name="nominal_pengeluaran" placeholder="Masukan Nominal Pengeluaran" value="{{ old('nominal_pengeluaran', $pengeluaran->nominal_pengeluaran) }}">
                        @error('nominal_pengeluaran')
                            <span class="invalid-feedback d-block" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
            
                <div class="form-group row">
                    <div class="col-sm-12 mb-2">
                        <label>Keterangan</label>
                        <textarea class="form-control" name="keterangan" placeholder="Masukan Keterangan">{{ old('keterangan', $pengeluaran->keterangan) }}</textarea>
                        @error('keterangan')
                            <span class="invalid-feedback d-block" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                                
                <button type="submit" class="btn btn-primary btn-icon-split">
                    <span class="icon text-white-50">
                        <i class="fas fa-save"></i>
                    </span>
                    <span class="text">Simpan</span>
                </button>
            </form>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
</div>