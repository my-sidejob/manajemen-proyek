@extends('layouts.app')

@section('title', 'Form Pengeluaran')

@section('content')

<div class="container-fluid">
  
    <!-- Content Row -->
    <div class="row">
        <div class="col-lg-8 mb-4">
            <div class="card">
                <div class="card-header">
                    Form Pengeluaran
                </div>
                <div class="card-body">
                    <form action="{{ (!isset($pengeluaran->id)) ?  route('pengeluaran.store') : route('pengeluaran.update', $pengeluaran->id) }}" method="post">
                        @csrf
        
                        @if(isset($pengeluaran->id))
                            @method('put')
                        @endif
        
                        <div class="form-group row">
                            <div class="col-sm-12 mb-2">
                                <label>Tanggal</label>
                                <input type="date" class="form-control form-control-user" name="tanggal" placeholder="Tanggal" value="{{ old('tanggal', $pengeluaran->tanggal) }}">
                                @error('tanggal')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-12 mb-2">
                                <label>Nama Pengeluaran</label>
                                <input type="text" class="form-control form-control-user" name="nama_pengeluaran"  placeholder="Masukan Nama Pengeluaran" value="{{ old('nama_pengeluaran', $pengeluaran->nama_pengeluaran) }}">
                                @error('nama_pengeluaran')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
        
                        <div class="form-group row">
                            <div class="col-sm-12 mb-2">
                                <label>Nominal Pengeluaran</label>
                                <input type="number" class="form-control form-control-user" name="nominal_pengeluaran" placeholder="Masukan Nominal Pengeluaran" value="{{ old('nominal_pengeluaran', $pengeluaran->nominal_pengeluaran) }}">
                                @error('nominal_pengeluaran')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    
                        <div class="form-group row">
                            <div class="col-sm-12 mb-2">
                                <label>Keterangan</label>
                                <textarea class="form-control" name="keterangan" placeholder="Masukan Keterangan">{{ old('keterangan', $pengeluaran->keterangan) }}</textarea>
                                @error('keterangan')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                                        
                        <button type="submit" class="btn btn-primary btn-icon-split">
                            <span class="icon text-white-50">
                                <i class="fas fa-save"></i>
                            </span>
                            <span class="text">Simpan</span>
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection