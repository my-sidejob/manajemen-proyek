@extends('layouts.app')

@section('title', 'Form User')

@section('content')

<div class="container-fluid">
  
    <!-- Content Row -->
    <div class="row">
        <div class="col-lg-8 mb-4">
            <div class="card">
                <div class="card-header">
                    Form user
                </div>
                <div class="card-body">
                    <form class="user" action="{{ (!isset($user->id)) ?  route('user.store') : route('user.update', $user->id) }}" method="post">
                        @csrf
        
                        @if(isset($user->id))
                            @method('put')
                        @endif
        
                        <div class="form-group row">
                            <div class="col-sm-6 mb-2">
                                <input type="text" class="form-control form-control-user" name="nama" placeholder="Nama User / Petugas" value="{{ old('nama', $user->nama) }}">
                                @error('nama')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col-sm-6 mb-2">
                                <input type="text" class="form-control form-control-user" name="username"  placeholder="username" value="{{ old('username', $user->username) }}">
                                @error('username')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
        
                        <div class="form-group row">
                            <div class="col-sm-6 mb-2">
                                <input type="text" class="form-control form-control-user" name="email" placeholder="Email" value="{{ old('email', $user->email) }}">
                                @error('email')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col-sm-6 mb-2">
                                <select class="form-control" name="jabatan" id="exampleFormControlSelect1">
                                    <option>Jabatan</option>
                                    <option value="1" {{ (old('jabatan') == '1' || $user->jabatan == '1') ? 'selected' : '' }}>Owner</option>
                                    <option value="2" {{ (old('jabatan') == '2' || $user->jabatan == '2') ? 'selected' : '' }}>Finance</option>
                                    <option value="3" {{ (old('jabatan') == '3' || $user->jabatan == '3') ? 'selected' : '' }}>CSA / Marketing</option>
                                    <option value="4" {{ (old('jabatan') == '4' || $user->jabatan == '4') ? 'selected' : '' }}>Produksi</option>
                                </select>
                                @error('jabatan')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>                    
                        </div>
                        @if(!isset($user->id))                           
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <input type="password" name="password" class="form-control" placeholder="Password">
                                    @error('password')
                                        <span class="invalid-feedback d-block" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                        @endif
                    
                        <button type="submit" class="btn btn-primary btn-icon-split">
                            <span class="icon text-white-50">
                                <i class="fas fa-save"></i>
                            </span>
                            <span class="text">Save</span>
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection