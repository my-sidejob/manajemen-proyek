<div class="modal-header">
    <h5 class="modal-title">Informasi Pemesanan</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
    <div class="row text-left">
        <div class="col-md-12">
            <table class="table table-sm">
                <tr>
                    <td style="border-top:0">No. INV</td>
                    <td style="border-top:0">:</td>
                    <td style="border-top:0">{{ $pemesanan->kode }}</td>
                </tr>
                <tr>
                    <td>Tanggal</td>
                    <td>:</td>
                    <td>{{ $pemesanan->tanggal }}</td>
                </tr>
                <tr>
                    <td>Konsumen</td>
                    <td>:</td>
                    <td>{{ $pemesanan->konsumen->nama_konsumen }}</td>
                </tr>
                <tr>
                    <td>Total</td>
                    <td>:</td>
                    <td>{{ number_format($pemesanan->total) }}</td>
                </tr>
                <tr>
                    <td>Status Pembayaran</td>
                    <td>:</td>
                    <td>{{ $pemesanan->textStatusPembayaran() }}</td>
                </tr>
                <tr>
                    <td>Sisa</td>
                    <td>:</td>
                    <td><?= number_format($sisa) ?></td>
                </tr>
            </table>
        </div>

        <div class="col-md-12">
            <p>Riwayat Pembayaran</p>
            <table class="table table-sm">
                <tr>
                    <th>#</th>
                    <th>Tanggal</th>
                    <th>Bukti</th>
                    <th>Nominal</th>                    
                </tr>
                @forelse($pembayaran as $row)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $row->tanggal_pembayaran }}</td>
                        <td>
                            @if($row->bukti_pembayaran != null)
                            <a href="{{ asset('storage/bukti_pembayaran/'.$row->bukti_pembayaran) }}" target="_blank">Bukti</a>
                            @else 
                            Tidak Tersedia
                            @endif
                        </td>
                        <td>{{ number_format($row->nominal) }}</td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="3" class="text-center">Belum ada riwayat pembayaran</td>
                    </tr>
                @endforelse
            </table>
        </div>

        @if($sisa != 0)
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Form Pembayaran
                </div>
                <div class="card-body">

                    <form action="{{ route('pembayaran.store', $pemesanan->id) }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="">Bukti Pembayaran</label>
                            <input type="file" name="bukti_pembayaran" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="">Masukan Nominal Pembayaran</label>
                            <input type="number" name="nominal" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <input type="submit" value="Simpan Pembayaran" class="btn btn-primary">
                        </div>
        
                    </form>
                </div>
            </div>
        </div>
        @endif
        
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
</div>