@extends('layouts.app')

@section('title', 'Data Jenis Cetak')

@section('content')

<div class="container-fluid">
  
    <!-- Content Row -->
    <div class="row">
      <div class="col-lg-12 mb-4">
        <!-- Card -->
        <div class="card shadow mb-4">            
            <div class="card-body">
                <a href="{{ route('jenis_cetak.create') }}" class="btn btn-primary mb-4"><i class="fa fa-plus"></i> Tambah</a>
                <table class="table table-striped" id="myTable">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nama Cetak</th>
                            <th>Harga</th>                            
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($jenis_cetak as $row)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $row->nama_cetak }}</td>
                            <td>{{ $row->harga }}</td>
                            <td>
                                <form action="{{ route('jenis_cetak.destroy', $row->id) }}" method="post">                                       
                                    <a href="{{ route('jenis_cetak.edit', $row->id) }}" class="btn btn-sm btn-warning" title="Edit"><i class="fa fa-cog"></i></a>
                                    @csrf
                                    @method('delete')
                                    <button type="submit" class="btn btn-sm btn-danger" onclick="return confirm('Yakin hapus data?')" >
                                        <span class="icon text-white-50">
                                        <i class="fas fa-trash"></i>
                                        </span>
                                    </button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>                
            </div>
        </div>
      </div>
    </div>

</div>
@endsection
