@extends('layouts.app')

@section('title', 'Form Jenis Cetak')

@section('content')

<div class="container-fluid">
  
    <!-- Content Row -->
    <div class="row">
        <div class="col-lg-8 mb-4">
            <div class="card">
                <div class="card-header">
                    Form Jenis Cetak
                </div>
                <div class="card-body">
                    <form class="user" action="{{ (!isset($jenis_cetak->id)) ?  route('jenis_cetak.store') : route('jenis_cetak.update', $jenis_cetak->id) }}" method="post">
                        @csrf
        
                        @if(isset($jenis_cetak->id))
                            @method('put')
                        @endif
        
                        <div class="form-group row">
                            <div class="col-sm-12 mb-2">
                                <label>Nama Cetak</label>
                                <input type="text" class="form-control form-control-user" name="nama_cetak" value="{{ old('nama_cetak', $jenis_cetak->nama_cetak) }}">
                                @error('nama_cetak')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-12 mb-2">
                                <label>Harga</label>
                                <input type="number" class="form-control form-control-user" name="harga" value="{{ old('harga', $jenis_cetak->harga) }}">
                                @error('harga')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        
                        <button type="submit" class="btn btn-primary btn-icon-split">
                            <span class="icon text-white-50">
                                <i class="fas fa-save"></i>
                            </span>
                            <span class="text">Simpan</span>
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection