<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{ url('dashboard')}}">
        
        <div class="sidebar-brand-text mx-3">Sistem Manajemen Proyek</div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item {{ (urlHasPrefix('dashboard') == true ) ? 'active' : '' }}">
        <a class="nav-link" href="{{ url('dashboard') }}">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span>
        </a>
    </li>

    @if(Auth::user()->jabatan != 4)
    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">
    <div class="sidebar-heading">
        Master
    </div>

    <li class="nav-item {{ (urlHasPrefix('konsumen') == true ) ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('konsumen.index') }}">
          <i class="fas fa-users"></i>
          <span>Konsumen / Customer</span></a>
    </li>
    @if(Auth::user()->jabatan == 1)
    <li class="nav-item {{ (urlHasPrefix('user') == true ) ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('user.index') }}">
          <i class="fas fa-user-tie"></i>
          <span>User / Petugas</span></a>
    </li>
    @endif
    <li class="nav-item {{ (urlHasPrefix('produk') == true ) ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('produk.index') }}">
          <i class="fas fa-box"></i>
          <span>Produk</span></a>
    </li>

    {{-- <li class="nav-item {{ (urlHasPrefix('jenis_cetak') == true ) ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('jenis_cetak.index') }}">
          <i class="fas fa-print"></i>
          <span>Jenis Cetak</span></a>
    </li> --}}

    @endif

     <!-- Divider -->
     <hr class="sidebar-divider d-none d-md-block">
     <div class="sidebar-heading">
         Transaksi
     </div>

     @if(Auth::user()->jabatan == 1 || Auth::user()->jabatan == 2)
     {{-- <li class="nav-item link-pengeluaran {{ (urlHasPrefix('pengeluaran') == true ) ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('pengeluaran.index') }}">
          <i class="fas fa-minus-square"></i>
          <span>Pengeluaran</span></a>
    </li> --}}
    @endif
    @if(Auth::user()->jabatan != 4)
    <li class="nav-item link-pemesanan {{ (urlHasPrefix('pemesanan') == true ) ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('pemesanan.index') }}">
          <i class="fas fa-shopping-cart"></i>
          <span>Pemesanan</span></a>
    </li>
    @endif
    <li class="nav-item link-data-pemesanan {{ (urlHasPrefix('show-pemesanan') == true ) ? 'active' : '' }}">
        <a class="nav-link collapsed" href="{{ route('pemesanan.show_all') }}" data-toggle="collapse" data-target="#collapseTwo">
          <i class="fas fa-dollar-sign"></i>
          <span>Data Pemesanan</span></a>
          
          <div id="collapseTwo" class="collapse {{ (urlHasPrefix('show-pemesanan') == true ) ? 'show' : '' }}" aria-labelledby="headingTwo" data-parent="#accordionSidebar" style="">
                <div class="bg-white py-2 collapse-inner rounded">
                    <a class="collapse-item {{ (urlHasPrefix('show-pemesanan/1') == true ) ? 'active' : '' }}" href="{{ url('show-pemesanan/1') }}">Pesanan Masuk</a>
                    <a class="collapse-item {{ (urlHasPrefix('show-pemesanan/2') == true ) ? 'active' : '' }}" href="{{ url('show-pemesanan/2') }}">Telah Dikonfirmasi</a>
                    <a class="collapse-item {{ (urlHasPrefix('show-pemesanan/3') == true ) ? 'active' : '' }}" href="{{ url('show-pemesanan/3') }}">Pemesanan Diproses</a>
                    <a class="collapse-item {{ (urlHasPrefix('show-pemesanan/4') == true ) ? 'active' : '' }}" href="{{ url('show-pemesanan/4') }}">Selesai dan Siap Diambil</a>
                    <a class="collapse-item {{ (urlHasPrefix('show-pemesanan/5') == true ) ? 'active' : '' }}" href="{{ url('show-pemesanan/5') }}">Pemesanan Telah Diambil</a>
                    <a class="collapse-item" href="{{ route('pemesanan.show_all') }}">Semua Pesanan</a>
                </div>
          </div>
    </li>

    @if(Auth::user()->jabatan == 1 || Auth::user()->jabatan == 2)
    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">
    <div class="sidebar-heading">
        Laporan
    </div>

    <li class="nav-item link-pemesanan {{ (urlHasPrefix('penjualan') == true ) ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('laporan.penjualan') }}">
          <i class="fas fa-file"></i>
          <span>Penjualan</span></a>
    </li>

    <li class="nav-item link-laporan-pengeluaran {{ (urlHasPrefix('pengeluaran') == true ) ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('laporan.pengeluaran') }}">
          <i class="fas fa-file"></i>
          <span>Pengeluaran</span></a>
    </li>

    <li class="nav-item link-laba-rugi {{ (urlHasPrefix('laba-rugi') == true ) ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('laporan.laba-rugi') }}">
          <i class="fas fa-file"></i>
          <span>Laba Rugi</span></a>
    </li>

    @endif

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>
</ul>