@extends('layouts.app')

@section('title', 'Proses Pemesanan')

@section('content')

<div class="container-fluid">
    <form action="{{ route('pemesanan.process') }}" method="post">
        @csrf
        <div class="row mb-4">
            <div class="col-md-7">
                <div class="card">
                    <div class="card-header">
                        Data Pemesanan
                    </div>
                    <div class="card-body">
                        <p class="text-danger">Mohon perhatikan data pemesanan dibawah.</p>
                        <table class="table">
                            <tr>
                                <td>Kode</td>
                                <td>:</td>
                                <td>{{ $kode }}</td>
                            </tr>
                            <tr>
                                <td>Nama Pemesan</td>
                                <td>:</td>
                                <td>{{ $nama_konsumen }}</td>
                            </tr>
                            <tr>
                                <td>Tanggal</td>
                                <td>:</td>
                                <td>{{ $tanggal }}</td>
                            </tr>
                            <tr>
                                <td>Jenis Cetak</td>
                                <td>:</td>
                                <td>{{ $nama_cetak }}</td>
                            </tr>
                            {{-- <tr>
                                <td>Harga Cetak</td>
                                <td>:</td>
                                <td>Rp. {{ number_format($harga_cetak) }}</td>
                            </tr> --}}
                            
                        </table>
                    </div>
                </div>
            </div>

            <div class="col-md-5">
                @if($design != null)
                    <h4>Design</h4>
                    <img src="{{ asset('storage/design/'.$design) }}" alt="design" class="img-fluid">
                @endif
            </div>
        </div>

        <div class="row mb-4">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        Item Pemesanan
                    </div>
                    <div class="card-body">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Produk</th>
                                    <th>Qty</th>
                                    <th>Harga</th>
                                    <th>Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $sub_total = 0; ?>
                                @for ($i = 0; $i < count($produk_id); $i++)
                                    <?php 
                                        $total = getProductDetail($produk_id[$i])->harga_satuan * $qty[$i];
                                        $sub_total += $total;
                                    ?>
                                    <input type="hidden" name="produk_id[]" value="{{ $produk_id[$i] }}">
                                    <input type="hidden" name="qty[]" value="{{ $qty[$i] }}">
                                    <tr>
                                        <td>{{ $i + 1 }}</td>
                                        <td>{{ getProductDetail($produk_id[$i])->nama_produk }}</td>
                                        <td>{{ $qty[$i] }}</td>
                                        <td>{{ number_format(getProductDetail($produk_id[$i])->harga_satuan) }}</td>
                                        <td>{{ number_format($total) }}</td>
                                    </tr>
                                @endfor
                                

                                <tr>
                                    <td colspan="3" class="text-right"><b>Total</b></td>
                                    <td class="text-center">:</td>
                                    <td>{{ number_format($sub_total + $harga_cetak) }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <input type="hidden" name="kode" value="{{ $kode }}">
        <input type="hidden" name="tanggal" value="{{ $tanggal }}">
        <input type="hidden" name="design" value="{{ $design }}">
        <input type="hidden" name="jenis_cetak_id" value="{{ $jenis_cetak_id }}">
        <input type="hidden" name="total" value="{{ $sub_total }}">
        <input type="hidden" name="status_pemesanan" value="1">
        <input type="hidden" name="konsumen_id" value="{{ $konsumen_id }}">
        <input type="hidden" name="user_id" value="{{ $user }}">
        

        <div class="row mb-4">
            <div class="col-md-12">
                <button type="submit" class="btn btn-primary btn-icon-split">
                    <span class="icon text-white-50">
                        <i class="fas fa-save"></i>
                    </span>
                    <span class="text">Simpan Pesanan</span>
                </button>
            </div>
        </div>

    </form>
</div>

@endsection