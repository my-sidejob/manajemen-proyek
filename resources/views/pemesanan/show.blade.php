@extends('layouts.app')

@section('title', 'Proses Pemesanan')

@section('content')

<div class="container-fluid">
        <div class="row mb-4">
            <div class="col-md-7">
                <div class="card">
                    <div class="card-header">
                        Data Pemesanan
                    </div>
                    <div class="card-body">
                        @if($pemesanan->status_pemesanan != 1 && $pemesanan->status_pembayaran != 1)
                            @if(Auth::user()->jabatan == 1 || Auth::user()->jabatan == 4)
                                <form action="{{ route('pemesanan.changeStatus', [$pemesanan->id, '3']) }}" method="post" class="d-inline-block">
                                    @csrf 
                                    @method('put')
                                    <button type="submit" href="" class="btn btn-sm btn-primary" {{ ($pemesanan->status_pemesanan > 2) ? 'disabled' : '' }} title="Proses Pesanan Ini">Proses Pesanan Ini</button>                                
                                </form>
                                @if($pemesanan->status_pemesanan > 2)
                                <form action="{{ route('pemesanan.changeStatus', [$pemesanan->id, '4']) }}" method="post" class="d-inline-block">
                                    @csrf 
                                    @method('put')
                                    <button type="submit" href="" class="btn btn-sm btn-primary" {{ ($pemesanan->status_pemesanan > 3) ? 'disabled' : '' }} title="Proses Pesanan Selesai">Proses Pesanan Selesai</button>                                
                                </form>
                                @endif
                                
                            @endif
                            @if($pemesanan->status_pemesanan >= 4)
                            <form action="{{ route('pemesanan.changeStatus', [$pemesanan->id, '5']) }}" method="post" class="d-inline-block">
                                @csrf 
                                @method('put')
                                <button type="submit" href="" class="btn btn-sm btn-success" {{ ($pemesanan->status_pemesanan > 4) ? 'disabled' : '' }} onclick="return confirm('Pastikan konsumen sudah membayar lunas?')"><i class="fa fa-check"></i> Pemesanan diambil oleh konsumen</button>                                
                            </form>
                            @endif

                        @endif

                        <br><br>
                        <table class="table">
                            <tr>
                                <td>Kode</td>
                                <td>:</td>
                                <td>{{ $pemesanan->kode }}</td>
                            </tr>
                            <tr>
                                <td>Nama Pemesan</td>
                                <td>:</td>
                                <td>{{ $pemesanan->konsumen->nama_konsumen }}</td>
                            </tr>
                            <tr>
                                <td>Tanggal</td>
                                <td>:</td>
                                <td>{{ $pemesanan->tanggal }}</td>
                            </tr>
                            <tr>
                                <td>Jenis Cetak</td>
                                <td>:</td>
                                <td>{{ $pemesanan->cetak->nama_cetak }}</td>
                            </tr>
                            {{-- <tr>
                                <td>Harga Cetak</td>
                                <td>:</td>
                                <td>Rp. {{ number_format($pemesanan->cetak->harga) }}</td>
                            </tr> --}}
                            
                        </table>
                    </div>
                </div>
            </div>

            <div class="col-md-5">
                @if($pemesanan->design != null)
                    <h4>Design</h4>
                    <img src="{{ asset('storage/design/'.$pemesanan->design) }}" alt="design" class="img-fluid">
                @endif
            </div>
        </div>

        <div class="row mb-4">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        Item Pemesanan
                    </div>
                    <div class="card-body">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Produk</th>
                                    <th>Qty</th>
                                    <th>Harga</th>
                                    <th>Total</th>
                                </tr>
                            </thead>
                            <tbody>                                
                                <?php $sub_total = 0; ?>
                                @foreach($pemesanan->details as $row)
                                    <?php 
                                        $total = $row->produk->harga_satuan * $row->jumlah;
                                        $sub_total += $total;
                                    ?>                                   
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $row->produk->nama_produk }}</td>
                                        <td>{{ $row->jumlah }}</td>
                                        <td>{{ $row->produk->harga_satuan }}</td>
                                        <td>{{ number_format($total) }}</td>
                                    </tr>
                                @endforeach
                                

                                <tr>
                                    <td colspan="3" class="text-right"><b>Total</b></td>
                                    <td class="text-center">:</td>
                                    <td>{{ number_format($sub_total + $pemesanan->cetak->harga) }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="row mb-4">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        Pengeluaran dalam pesanan ini
                    </div>
                    <div class="card-body">
                        @if(Auth::user()->jabatan == 1 || Auth::user()->jabatan == 4)
                        <a href="{{ route('pengeluaran.modal', $pemesanan->id) }}" class="btn btn-sm btn-primary mb-3 btn-pengeluaran">Tambah Pengeluaran</a>
                        @endif
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Tanggal</th>
                                    <th>Nama Pengeluaran</th>
                                    <th>Nominal</th>
                                    <th>Keterangan</th>
                                    <th>Petugas</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $pengeluaran = 0; ?>
                                @forelse($pemesanan->pengeluaran as $row)
                                <?php $pengeluaran += $row->nominal_pengeluaran; ?>
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $row->tanggal }}</td>
                                    <td>{{ $row->nama_pengeluaran }}</td>
                                    <td>{{ number_format($row->nominal_pengeluaran) }}</td>
                                    <td>{{ $row->keterangan }}</td>
                                    <td>{{ ucwords($row->user->nama) }}</td>                            
                                </tr>                                
                                @empty 
                                <tr>
                                    <td colspan="5">Belum ada pengeluaran untuk pesanan ini </td>
                                </tr>
                                @endforelse
                                <tr>
                                    <td colspan="2" class="text-right"><b>Total Pengeluaran</b></td>
                                    <td class="text-center">:</td>
                                    <td colspan="3">{{ number_format($pengeluaran) }}</td>
                                </tr>
                            </tbody>
                        </table>

                        <hr>
                        <?php 
                            $pemasukan = $sub_total + $pemesanan->cetak->harga;
                            $laba = $pemasukan-$pengeluaran;
                        ?>
                        <p>Total Pemasukan: Rp. {{ number_format($pemasukan) }}</p>                        
                        <p>Total Pengeluaran: Rp. {{ number_format($pengeluaran) }}</p>
                        <p>Laba: {{ number_format(($sub_total + $pemesanan->cetak->harga) - $pengeluaran) }} ({{ round(($laba/$pemasukan) * 100) }}%)</p>
                    </div>
                </div>
            </div>
        </div>
        
</div>

<div id="theModal" class="modal fade text-center">
    <div class="modal-dialog modal-lg">
        <div class="modal-content modal-lg">
        </div>
    </div>
</div>


@endsection

@push('scripts')

<script>
    $('.btn-pengeluaran').on('click', function(e){
        e.preventDefault();
        $('#theModal').modal('show').find('.modal-content').load($(this).attr('href'));
    });
</script>

@endpush