@extends('layouts.app')

@section('title', 'Form Pemesanan')

@section('content')

<div class="container-fluid">
  
    <!-- Content Row -->
    <form action="{{ (!isset($pemesanan->id)) ?  route('pemesanan.store') : route('pemesanan.update', $pemesanan->id) }}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="row">
            <div class="col-lg-8 mb-4">
                <div class="card">
                    <div class="card-header">
                        Masukan Data Pemesanan
                    </div>
                    <div class="card-body">
                            @csrf
            
                            @if(isset($pemesanan->id))
                                @method('put')
                            @endif
            
                            <div class="form-group row">
                                <div class="col-sm-12 mb-2">
                                    <label>Tanggal</label>
                                    <input type="date" class="form-control form-control-user" name="tanggal" placeholder="Tanggal" value="{{ old('tanggal', $pemesanan->tanggal) }}">
                                    @error('tanggal')
                                        <span class="invalid-feedback d-block" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-12 mb-2">                            
                                    <label>Konsumen <small>(klik <a href="{{ route('konsumen.create') }}" target="_blank" style="color: blue">disini</a> jika konsumen tidak ditemukan. Setelah menambah konsumen refresh halaman ini.)</small></label>
                                    <select name="konsumen_id" class="mySelect form-control">
                                        <option value=""> - Pilih Konsumen - </option>
                                        @foreach($konsumen as $option)
                                            <option value="{{ $option->id }}" {{ old('konsumen_id') == $option->id ? 'selected' : '' }}>{{ ucwords($option->nama_konsumen) }}</option>                         
                                        @endforeach
                                    </select>
                                    @error('konsumen_id')
                                        <span class="invalid-feedback d-block" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror                           
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-12 mb-2">                            
                                    <label>Jenis Cetak</label>
                                    <select name="jenis_cetak_id" class="form-control">
                                        <option value=""> - Pilih jenis cetak -</option>
                                        @foreach($jenis_cetak as $option)
                                            <option value="{{ $option->id }}" {{ old('jenis_cetak_id') == $option->id ? 'selected' : '' }}>{{ ucwords($option->nama_cetak) }}</option>
                                        @endforeach
                                    </select>
                                    @error('jenis_cetak_id')
                                        <span class="invalid-feedback d-block" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror                           
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-12 mb-2">                            
                                    <label>Design</label>
                                    <input type="file" name="design" class="form-control">
                                    @error('design')
                                        <span class="invalid-feedback d-block" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror                           
                                </div>
                            </div>
                            
                            
                        </div>
                    </div>
            </div>
        </div>
        
        <div class="row mb-4">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        Masukan Produk
                    </div>
                    <div class="card-body">
                        <div class="el_product">
                            <div class="row row_product mb-3">
                                <div class="col-md-7">
                                    <select name="produk_id[]" class="form-control produk_id" required>
                                        <option value="">Pilih Produk</option>
                                        @foreach($products as $produk)
                                            <option value="{{ $produk->id }}">{{ ucwords($produk->nama_produk) }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <input type="number" name="qty[]" class="form-control qty" placeholder="qty" required>
                                </div>                                
                                <div class="col-md-2">
                                    <a href="#" class="btn btn-danger btn-sm btn-del disabled"><i class="fa fa-minus"></i></a>
                                </div>
                            </div>                            
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <a href="#" class="link btn-add text-info" style="text-decoration: underline;">Tambah</a>
                            </div>
                        </div>
                        <hr>                        
                 
                        <div class="row">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary btn-icon-split">
                                    <span class="icon text-white-50">
                                        <i class="fas fa-save"></i>
                                    </span>
                                    <span class="text">Buat Pesanan</span>
                                </button>
                            </div>
                        </div>
                    </div>                  
                </div>
            </div>
        </div>
    </form>
</div>

@endsection

@push('scripts')

<script>
    $(document).ready(function(){
        var i = 1;
        $('.btn-add').click(function(e){
            e.preventDefault();
            var clone_el = `<div class="row row_product row_product_`+i+` mb-3">
                                <div class="col-md-7">
                                    <select name="produk_id[]" class="form-control produk_id_`+i+`" required>
                                        <option value="">Pilih Produk</option>
                                        @foreach($products as $produk)
                                            <option value="{{ $produk->id }}" data-harga="{{ $produk->harga_satuan }}">{{ ucwords($produk->nama_produk) }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <input type="number" name="qty[]" class="form-control qty" placeholder="qty" required>
                                </div>
                                
                                <div class="col-md-2">
                                    <a href="#" class="btn btn-danger btn-sm btn-del `+i+`"><i class="fa fa-minus"></i></a>
                                </div>
                            </div>`;

            $('.el_product').append(clone_el);
            
            $('.btn-del').click(function(e){
                e.preventDefault();
                var el_selected = $(this).attr('class');
                var number = el_selected.split(' ');
                $('.row_product_'+number[4]).remove();
                //alert(number[4]);
            });
                        
            i++;
        });
                
    });
</script>

@push('scripts')

<script>

    $(document).ready(function(){
        $('.link-data-pemesanan').removeClass('active');
    });

</script>

@endpush

@endpush