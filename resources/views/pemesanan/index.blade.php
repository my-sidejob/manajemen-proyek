@extends('layouts.app')

@section('title')
    Data Pemesanan {{ (isset($number)) ? showStatus($number) : '' }}
@endsection

@section('content')

<div class="container-fluid">
  
    <!-- Content Row -->
    <div class="row">
      <div class="col-lg-12 mb-4">
        <!-- Card -->
        <div class="card shadow mb-4">            
            <div class="card-body">

                <table class="table table-striped" id="myTable">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Invoice</th>
                            <th>Tanggal</th>
                            <th>Konsumen</th>
                            <th>Total</th>
                            <th>Status Pemesanan</th>
                            <th>Status Pembayaran</th>                            
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($pemesanan as $row)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $row->kode }}</td>
                            <td>{{ $row->tanggal }}</td>
                            <td>{{ $row->konsumen->nama_konsumen }}</td>
                            <td>{{ number_format($row->total) }}</td>
                            <td>{{ $row->textStatusPemesanan() }}</td>
                            <td>{{ $row->textStatusPembayaran() }}</td>
                            <td style="min-width: 112px;">
                                @if(Auth::user()->jabatan != 4)
                                    @if(Auth::user()->jabatan == 1 || Auth::user()->jabatan == 2)
                                    <form action="{{ route('pemesanan.changeStatus', [$row->id, 2]) }}" method="post" class="d-inline-block">
                                        @csrf 
                                        @method('put')
                                        <button type="submit" href="" class="btn btn-sm btn-primary" title="Konfirmasi Pesanan" {{ ($row->status_pemesanan > 1) ? 'disabled' : '' }}><i class="fa fa-thumbs-up"></i></button>                                
                                    </form>
                                    @endif
                                    @if(Auth::user()->jabatan != 3)
                                    <a href="{{ route('pembayaran', $row->id) }}" class="btn btn-sm btn-success btn-pay" title="Lihat Pembayaran"><i class="fa fa-dollar-sign"></i></a>
                                    @endif
                                @endif
                                <a href="{{ route('pemesanan.show', $row->id) }}" class="btn btn-sm btn-info" title="Lihat Detail Pemesanan"><i class="fa fa-eye"></i></a>
                                @if(Auth::user()->jabatan == 1 || Auth::user()->jabatan == 2 || Auth::user()->jabatan == 3)
                                    @if(($row->status_pemesanan == 1 || $row->status_pemesanan == 2) && $row->status_pembayaran == 1)
                                    <a href="{{ route('pemesanan.cancel', $row->id) }}" class="btn btn-sm btn-danger" title="Cancel Pemesanan"><i class="fa fa-times"></i></a>
                                
                                    @endif
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>                
            </div>
        </div>
      </div>
    </div>

</div>

<div id="theModal" class="modal fade text-center">
    <div class="modal-dialog modal-lg">
        <div class="modal-content modal-lg">
        </div>
    </div>
</div>

@endsection


@push('scripts')

<script>

    $(document).ready(function(){
        $('.link-pemesanan').removeClass('active');
    });

</script>

<script>
    $('.btn-pay').on('click', function(e){
        e.preventDefault();
        $('#theModal').modal('show').find('.modal-content').load($(this).attr('href'));
    });
</script>

@endpush