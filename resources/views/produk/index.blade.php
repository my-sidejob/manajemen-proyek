@extends('layouts.app')

@section('title', 'Data Produk')

@section('content')

<div class="container-fluid">
  
    <!-- Content Row -->
    <div class="row">
      <div class="col-lg-12 mb-4">
        <!-- Card -->
        <div class="card shadow mb-4">            
            <div class="card-body">
                <a href="{{ route('produk.create') }}" class="btn btn-primary mb-4"><i class="fa fa-plus"></i> Tambah</a>
                <table class="table table-striped" id="myTable">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nama Produk</th>
                            <th>Bahan Baku</th>
                            <th>Harga</th>
                            <th>Kategori</th>
                            <th>Keterangan</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($products as $row)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $row->nama_produk }}</td>
                            <td>{{ $row->bahan_baku }}</td>
                            <td>{{ $row->harga_satuan }}</td>
                            <td>{{ $row->kategori }}</td>
                            <td>{{ $row->keterangan }}</td>
                            <td>
                                <form action="{{ route('produk.destroy', $row->id) }}" method="post">                                       
                                    <a href="{{ route('produk.edit', $row->id) }}" class="btn btn-sm btn-warning" title="Edit"><i class="fa fa-cog"></i></a>
                                    @csrf
                                    @method('delete')
                                    {{-- <button type="submit" class="btn btn-sm btn-danger" onclick="return confirm('Yakin hapus data?')" >
                                        <span class="icon text-white-50">
                                        <i class="fas fa-trash"></i>
                                        </span>
                                    </button> --}}
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>                
            </div>
        </div>
      </div>
    </div>

</div>
@endsection
