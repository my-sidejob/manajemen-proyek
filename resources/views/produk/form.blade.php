@extends('layouts.app')

@section('title', 'Form Produk')

@section('content')

<div class="container-fluid">
  
    <!-- Content Row -->
    <div class="row">
        <div class="col-lg-8 mb-4">
            <div class="card">
                <div class="card-header">
                    Form Produk
                </div>
                <div class="card-body">
                    <form class="user" action="{{ (!isset($produk->id)) ?  route('produk.store') : route('produk.update', $produk->id) }}" method="post">
                        @csrf
        
                        @if(isset($produk->id))
                            @method('put')
                        @endif
        
                        <div class="form-group row">
                            <div class="col-sm-12 mb-2">
                                <label>Nama Produk</label>
                                <input type="text" class="form-control form-control-user" name="nama_produk" placeholder="Nama Produk" value="{{ old('nama_produk', $produk->nama_produk) }}">
                                @error('nama_produk')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-12 mb-2">
                                <label>Bahan Baku</label>
                                <input type="text" class="form-control form-control-user" name="bahan_baku"  placeholder="Bahan Baku" value="{{ old('bahan_baku', $produk->bahan_baku) }}">
                                @error('bahan_baku')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
        
                        <div class="form-group row">
                            <div class="col-sm-12 mb-2">
                                <label>Harga</label>
                                <input type="number" class="form-control form-control-user" name="harga_satuan" placeholder="Harga" value="{{ old('harga_satuan', $produk->harga_satuan) }}">
                                @error('harga_satuan')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-12 mb-2">
                                <label>Kategori</label>
                                <select class="form-control" name="kategori" id="exampleFormControlSelect1">
                                    <option value="">Pilih Kategori</option>
                                    <option value="seragam" {{ (old('kategori') == 'seragam' || $produk->kategori == 'seragam') ? 'selected' : '' }}>Seragam</option>
                                    <option value="souvenir" {{ (old('kategori') == 'souvenir' || $produk->kategori == 'souvenir') ? 'selected' : '' }}>Souvenir</option>                                    
                                </select>
                                @error('kategori')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>                    
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-12 mb-2">
                                <label>Keterangan</label>
                                <textarea class="form-control" name="keterangan" placeholder="Keterangan">{{ old('keterangan', $produk->keterangan) }}</textarea>
                                @error('keterangan')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                                        
                        <button type="submit" class="btn btn-primary btn-icon-split">
                            <span class="icon text-white-50">
                                <i class="fas fa-save"></i>
                            </span>
                            <span class="text">Simpan</span>
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection