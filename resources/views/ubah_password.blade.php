@extends('layouts.app')

@section('title', 'Ubah Password')

@section('content')

<div class="container-fluid">
    <div class="row">
        <!-- Content Column -->
        <div class="col-lg-6 mb-4">
            
            <!-- Card -->
            <div class="card shadow mb-4">
              <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Form ubah password</h6>
              </div>
              <div class="card-body">
                
        
                  <form class="user" method="post" action="{{ route('ubah_password.change') }}">
                      @csrf 
                      
                      <p>Old Password</p>
                      <div class="form-group row">
                          <div class="col-sm-6 mb-2">
                              <input type="password" name="old_password" class="form-control form-control-user" id="opassword" placeholder="Old Password">
                              @if(session()->has('failed_password'))                    
                                  <span class="invalid-feedback d-block" role="alert">
                                      <strong>{{ session()->get('failed_password') }}</strong>
                                  </span>
                              @enderror
                          </div>
                      </div>
          
                      <p>New Password</p>
                      <div class="form-group row">
                          <div class="col-sm-6 mb-2">
                              <input type="password" class="form-control form-control-user" name="password" id="npassword" placeholder="New Password">
                              @error('password')
                                  <span class="invalid-feedback d-block" role="alert">
                                      <strong>{{ $message }}</strong>
                                  </span>
                              @enderror
                          </div>
                          <div class="col-sm-6 mb-2">
                              <input type="password" class="form-control form-control-user" name="password_confirmation" id="nrpassword" placeholder="Repeat New Password">
                              @error('password_confirmation')
                                  <span class="invalid-feedback d-block" role="alert">
                                      <strong>{{ $message }}</strong>
                                  </span>
                              @enderror
                          </div>
                      </div>
          
                      <button type="submit" href="#" class="btn btn-primary btn-icon-split">
                          <span class="icon text-white-50">
                              <i class="fas fa-save"></i>
                          </span>
                          <span class="text">Change Password</span>
                      </button>
                  </form>
        
              </div>
            </div>
        
        </div>
    </div>

</div>

@endsection
