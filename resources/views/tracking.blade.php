@extends('layouts.app')

@section('title', 'Lacak Pesanan')

@section('content')

<body class="bg-gradient-primary">

    <div class="container">
  
        <!-- Outer Row -->
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="card o-hidden border-0 shadow-lg my-5">
                    <div class="card-header">
                        Proses Pemesanan {{ $pemesanan->kode }}
                    </div>
                    <div class="card-body">
                        @foreach(listStatusPemesanan() as $key => $list)

                        @if($key > $pemesanan->status_pemesanan)
                        <div class="card mb-4 border-left-primary" style="background-color: #d7d7db!important;">
                        @else
                        <div class="card mb-4 border-left-primary">
                        @endif   
                            <div class="card-body">
                              {{ $list }}
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

@endsection