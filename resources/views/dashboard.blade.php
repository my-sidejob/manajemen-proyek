@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')

<div class="container-fluid">

    <div class="row">
      <div class="col-md-3">
        <div class="card border-left-info shadow h-100 py-2">
          <div class="card-body">
            <div class="row no-gutters align-items-center">
              <div class="col mr-2">
                <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Pesanan Masuk</div>
                <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $pesanan_masuk }} Pesanan</div>
              </div>
              <div class="col-auto">
                <i class="fas fa-boxes fa-2x text-gray-300"></i>
              </div>
            </div>
          </div>
        </div>
      </div>      

      <div class="col-md-3">
        <div class="card border-left-warning shadow h-100 py-2">
          <div class="card-body">
            <div class="row no-gutters align-items-center">
              <div class="col mr-2">
                <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Sedang Dikerjakan</div>
                <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $sedang_dikerjakan }} Pesanan</div>
              </div>
              <div class="col-auto">
                <i class="fa fa-hands fa-2x text-gray-300"></i>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="col-md-3">
        <div class="card border-left-success shadow h-100 py-2">
          <div class="card-body">
            <div class="row no-gutters align-items-center">
              <div class="col mr-2">
                <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Pesanan Selesai</div>
                <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $pesanan_selesai }} Pesanan</div>
              </div>
              <div class="col-auto">
                <i class="fas fa-thumbs-up fa-2x text-gray-300"></i>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="col-md-3">
        <div class="card border-left-primary shadow h-100 py-2">
          <div class="card-body">
            <div class="row no-gutters align-items-center">
              <div class="col mr-2">
                <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Total Pemesanan</div>
                <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $total_pemesanan }} Pesanan</div>
              </div>
              <div class="col-auto">
                <i class="fas fa-shopping-cart fa-2x text-gray-300"></i>
              </div>
            </div>
          </div>
        </div>
      </div>
     
    </div>

    <div class="row mb-4 mt-4">
      <div class="col-md-6">
         <!-- Card -->
         <div class="card shadow mb-4">
          <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Dashboard</h6>
          </div>
          <div class="card-body">
              <p>Selamat Datang di Sistem Manajemen Proyek Pada CV. Cahaya Bintang Gemilang.</p>                
          </div>
        </div>

        <div class="card shadow mb-4">
          <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Pesanan Masuk</h6>
          </div>
          <div class="card-body">
            <table class="table table-striped" id="myTable">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Inv.</th>
                        <th>Tanggal</th>
                        <th>Konsumen</th>
                        <th>Total</th>
                        <th>Status</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($pemesanan as $row)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $row->kode }}</td>
                        <td>{{ $row->tanggal }}</td>
                        <td>{{ $row->konsumen->nama_konsumen }}</td>
                        <td>{{ number_format($row->total) }}</td>
                        <td>{{ $row->textStatusPemesanan() }}</td>
                        <td>
                            @if(Auth::user()->jabatan != 4)
                                @if(Auth::user()->jabatan == 1 || Auth::user()->jabatan == 2)
                                <form action="{{ route('pemesanan.changeStatus', [$row->id, 2]) }}" method="post" class="d-inline-block">
                                    @csrf 
                                    @method('put')
                                    <button type="submit" href="" class="btn btn-sm btn-primary" title="Konfirmasi Pesanan" {{ ($row->status_pemesanan > 1) ? 'disabled' : '' }}><i class="fa fa-thumbs-up"></i></button>                                
                                </form>
                                @endif                                
                            @endif                            
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>                
          </div>
        </div>

        
      </div>
      @if(Auth::user()->jabatan == 1 || Auth::user()->jabatan == 2)
      <div class="col-md-6">
        <div class="card">
          <div class="card-header">
            Grafik Jumlah Penjualan Setiap Bulan
          </div>
          <div class="card-body">
            <canvas id="myChart" width="400" height="400"></canvas>
          </div>
        </div>
      </div>
      @endif
    </div>
  

</div>
@endsection

@push('scripts')
<script src="{{ asset('vendor/chart.js/Chart.min.js') }}"></script>


<script>
  //console.log('{!! json_encode($tanggal) !!}');
  var ctx = document.getElementById('myChart').getContext('2d');
  var myChart = new Chart(ctx, {
      type: 'bar',
      data: {
          labels: {!! json_encode($tanggal) !!},
          datasets: [{
              label: ' Jml Penjualan',
              data: {!! json_encode($jumlah) !!},
              backgroundColor:'rgba(54, 162, 235, 0.2)',                  
            
              borderColor: 'rgba(54, 162, 235, 1)',
                 
              borderWidth: 1
          }]
      },
      options: {
          scales: {
              yAxes: [{
                  ticks: {
                      beginAtZero: true
                  }
              }]
          }
      }
  });
  </script>

@endpush