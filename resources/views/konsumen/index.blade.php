@extends('layouts.app')

@section('title', 'Konsumen')

@section('content')

<div class="container-fluid">
  
    <!-- Content Row -->
    <div class="row">
      <div class="col-lg-12 mb-4">
        <!-- Card -->
        <div class="card shadow mb-4">            
            <div class="card-body">
                <a href="{{ route('konsumen.create') }}" class="btn btn-primary mb-4"><i class="fa fa-plus"></i> Tambah</a>
                <table class="table table-striped" id="myTable">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nama</th>
                            <th>Instansi</th>
                            <th>Email</th>
                            <th>No Telp</th>
                            <th>Alamat</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($konsumen as $row)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $row->nama_konsumen }}</td>
                            <td>{{ $row->instansi }}</td>
                            <td>{{ $row->email }}</td>
                            <td>{{ $row->no_telp }}</td>
                            <td>{{ $row->alamat }}</td>
                            <td>
                                <form action="{{ route('konsumen.destroy', $row->id) }}" method="post">                                       
                                    <a href="{{ route('konsumen.edit', $row->id) }}" class="btn btn-sm btn-warning" title="Edit"><i class="fa fa-cog"></i></a>
                                    @csrf
                                    @method('delete')
                                    {{-- <button type="submit" class="btn btn-sm btn-danger" onclick="return confirm('Yakin hapus data?')" >
                                        <span class="icon text-white-50">
                                        <i class="fas fa-trash"></i>
                                        </span>
                                    </button> --}}
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>                
            </div>
        </div>
      </div>
    </div>

</div>
@endsection
