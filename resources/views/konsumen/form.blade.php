@extends('layouts.app')

@section('title', 'Konsumen')

@section('content')

<div class="container-fluid">
  
    <!-- Content Row -->
    <div class="row">
        <div class="col-lg-12 mb-4">
            <div class="card">
                <div class="card-header">
                    Form Konsumen
                </div>
                <div class="card-body">
                    <form class="user" action="{{ (!isset($konsumen->id)) ?  route('konsumen.store') : route('konsumen.update', $konsumen->id) }}" method="post">
                        @csrf
        
                        @if(isset($konsumen->id))
                            @method('put')
                        @endif
        
                        <div class="form-group row">
                            <div class="col-sm-6 mb-2">
                                <input type="text" class="form-control form-control-user" name="nama_konsumen" placeholder="Nama Konsumen" value="{{ old('nama_konsumen', $konsumen->nama_konsumen) }}">
                                @error('nama_konsumen')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col-sm-6 mb-2">
                                <input type="text" class="form-control form-control-user" name="instansi" id="lname" placeholder="Instansi" value="{{ old('instansi', $konsumen->instansi) }}">
                                @error('instansi')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
        
                        <div class="form-group row">
                            <div class="col-sm-6 mb-2">
                                <input type="text" class="form-control form-control-user" name="email" placeholder="Email" value="{{ old('email', $konsumen->email) }}">
                                @error('email')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col-sm-6 mb-2">
                                <input type="number" class="form-control form-control-user" name="no_telp" placeholder="No. Telp" value="{{ old('no_telp', $konsumen->no_telp) }}">
                                @error('no_telp')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>                    
                        </div>
                        <div class="form-group row">
                            <div class="col-md-12">
                                <textarea name="alamat" class="form-control" placeholder="Alamat">{{ old('alamat', $konsumen->alamat) }}</textarea>
                            </div>
                        </div>
                    
                        <button type="submit" class="btn btn-primary btn-icon-split">
                            <span class="icon text-white-50">
                                <i class="fas fa-save"></i>
                            </span>
                            <span class="text">Save</span>
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection