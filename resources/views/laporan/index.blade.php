@extends('layouts.app')

@section('title')
Laporan {{ ucwords($type) }}
@endsection

@section('content')

<div class="container-fluid">
  
    <!-- Content Row -->
    <div class="row">
        <div class="col-lg-12 mb-4">
            <div class="card">
                <div class="card-header">
                    Periode Laporan
                </div>
                <div class="card-body">
                    @if($type == 'penjualan')
                    <form action="{{ route('laporan.penjualan') }}" method="get">
                    @elseif($type == 'pengeluaran')
                    <form action="{{ route('laporan.pengeluaran') }}" method="get">
                    @elseif($type == 'laba rugi')
                    <form action="{{ route('laporan.laba-rugi') }}" method="get">
                    @endif
                        <div class="row">
                            <div class="col-md-6">
                                <label for="">Dari Tanggal</label>
                                <input type="date" name="start" value="{{ $start_date }}" class="form-control" placeholder="Dari Tanggal">
                            </div>

                            <div class="col-md-6">
                                <label for="">Sampai Tanggal</label>
                                <input type="date" name="end" value="{{ $end_date }}" class="form-control" placeholder="Sampai Tanggal">
                            </div>
                            
                            <div class="col-md-12 mt-3">
                                <button type="submit" class="btn btn-primary" name="search">Tampilkan Laporan</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    @if($start_date != '')

        @if($type == 'penjualan')
            @include('laporan.laporan_penjualan')
        @elseif($type == 'pengeluaran')
            @include('laporan.laporan_pengeluaran')
        @elseif($type == 'laba rugi')
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            Laporan Laba Rugi
                        </div>
                        <div class="card-body">
                            <p>Berikut laporan laba rugi periode {{ $start_date }} sampai {{ $end_date }}</p>
                            <table class="table">
                                <tr>
                                    <td>Total Pemasukan</td>
                                    <td>:</td>
                                    <td>{{ number_format($data['pemasukan']) }}</td>
                                </tr>
                                <tr>
                                    <td>Total Pengeluaran</td>
                                    <td>:</td>
                                    <td>{{ number_format($data['pengeluaran']) }}</td>
                                </tr>
                                @if($data['laba_rugi'] > 0)
                                <tr>
                                    <td>Total Laba</td>
                                    <td>:</td>
                                    <td>{{ number_format($data['laba_rugi']) }}</td>
                                </tr>
                                @else 
                                <tr>
                                    <td>Total Rugi</td>
                                    <td>:</td>
                                    <td>{{ number_format($data['laba_rugi']) }}</td>
                                </tr>
                                @endif
                            </table>
                            @if($data['laba_rugi'] > 0)
                            <p>Persentase keuntungan {{ $data['persentase'] }}% dari total pendapatan</p>
                            @else
                            <p>Persentase kerugian {{ $data['persentase'] }}% dari total pendapatan</p>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        @endif

    @endif
</div>

@endsection


@push('scripts')

<script>
    $('.link-pengeluaran').removeClass('active');
</script>

@endpush