<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                Laporan Pengeluaran dari tanggal {{ $start_date }} sampai {{ $end_date }}
            </div>
            <div class="card-body">

                <p>Berikut adalah laporan pengeluaran</p>

                <table class="table table-sm" id="myTable">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Tanggal</th>
                            <th>Nama Pengeluaran</th>
                            <th>Nominal</th>
                            <th>INV</th>
                            <th>Keterangan</th>
                            <th>Petugas</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $pengeluaran = 0; ?>
                        @foreach($data as $row)
                        <?php $pengeluaran += $row->nominal_pengeluaran; ?>
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $row->tanggal }}</td>
                            <td>{{ $row->nama_pengeluaran }}</td>
                            <td>{{ number_format($row->nominal_pengeluaran) }}</td>
                            <td>{{ $row->pemesanan->kode }}</td>
                            <td>{{ $row->keterangan }}</td>
                            <td>{{ ucwords($row->user->nama) }}</td> 
                            <td style="min-width: 30px;">                                    
                                <a href="{{ route('pemesanan.show', $row->pemesanan->id) }}" class="btn btn-sm btn-info" title="Lihat Detail Pemesanan"><i class="fa fa-eye"></i></a>                                   
                            </td>                           
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <hr>
               
                    <p>Total Pengeluaran: Rp. {{ number_format($pengeluaran) }}</p>
               
            </div>
        </div>
    </div>
</div>