<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                Laporan Penjualan dari tanggal {{ $start_date }} sampai {{ $end_date }}
            </div>
            <div class="card-body">
                @if($type == 'penjualan')
                    <p>Laporan penjualan dengan status pemesanan selesai atau sudah diambil dan pembayaran lunas.</p>
                @endif
                <table class="table table-sm" id="myTable">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Invoice</th>
                            <th>Tanggal</th>
                            <th>Konsumen</th>
                            <th>Total</th>
                            <th>Status Pemesanan</th>
                            <th>Status Pembayaran</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $omset = 0; ?>
                        @foreach($data as $row)
                        <?php $omset += $row->total; ?>
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $row->kode }}</td>
                            <td>{{ $row->tanggal }}</td>
                            <td>{{ $row->konsumen->nama_konsumen }}</td>
                            <td>{{ number_format($row->total) }}</td>
                            <td>{{ $row->textStatusPemesanan() }}</td>
                            <td>{{ $row->textStatusPembayaran() }}</td>
                            <td style="min-width: 112px;">                                    
                                <a href="{{ route('pemesanan.show', $row->id) }}" class="btn btn-sm btn-info" title="Lihat Detail Pemesanan"><i class="fa fa-eye"></i></a>                                   
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <hr>
                @if($type == 'penjualan')
                    <p>Total Omset: Rp. {{ number_format($omset) }}</p>
                @endif
            </div>
        </div>
    </div>
</div>