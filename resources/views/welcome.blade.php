@extends('layouts.app')

@section('title', 'Welcome')

@section('content')

<body class="bg-gradient-primary">

    <div class="container">
  
        <!-- Outer Row -->
        <div class="row justify-content-center">
            
            <div class=" col-md-6">
                <div class="card o-hidden border-0 shadow-lg my-5">
                    <div class="card-body p-0">
                        <!-- Nested Row within Card Body -->

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="p-4">
                                    <div class="text-center">
                                        <h1 class="h4 text-gray-900 mb-4">Selamat Datang!</h1>
                                        <hr>
                                        <p>Silahkan masukan nomor invoice pesanan anda untuk melacak proses pesanan anda.</p>
                                        @if(session()->has('error'))
                                        <hr>
                                        <p class="text-danger">{{ session()->get('error') }}</p>
                                        @endif
                                    </div>
                                    <form class="user" action="{{ route('search-pesanan') }}" method="post">
                                        @csrf
                                        <div class="form-group">
                                            <input type="text" name="kode" class="form-control form-control-user" id="exampleInputkode" aria-describedby="kodeHelp" placeholder="Contoh: INV-1-xxxxx">
                                            @error('kode')
                                                <span class="invalid-feedback d-block" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>                                        
                                        
                                        <div class="text-right">
                                            <button type="submit" class="btn btn-primary btn-user">
                                                Cari Pesanan
                                            </button>                    
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

@endsection