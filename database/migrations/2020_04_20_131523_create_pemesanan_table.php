<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePemesananTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pemesanan', function (Blueprint $table) {
            $table->id();
            $table->string('kode');
            $table->date('tanggal');
            $table->string('design')->nullable();
            $table->bigInteger('jenis_cetak_id')->unsigned();
            $table->bigInteger('total');
            $table->integer('status_pemesanan');
            $table->integer('status_pembayaran')->default(1);
            $table->bigInteger('konsumen_id')->unsigned();
            $table->bigInteger('user_id')->unsigned();        
            $table->timestamps();

            $table->foreign('konsumen_id')->references('id')->on('konsumen');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('jenis_cetak_id')->references('id')->on('jenis_cetak');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pemesanan');
    }
}
