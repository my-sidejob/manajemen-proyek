<?php

use App\Pemesanan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::middleware('auth')->group(function(){
    Route::get('dashboard', 'DashboardController@index')->name('dashboard');
    Route::resource('konsumen', 'KonsumenController');
    Route::resource('user', 'UserController')->middleware('owner-only');
    Route::resource('produk', 'ProdukController');
    Route::resource('jenis_cetak', 'JenisCetakController');
    //pengeluaran    
    Route::resource('pengeluaran', 'PengeluaranController')->middleware('top-level');
    Route::get('modal-pengeluaran/{pemesanan_id}', 'PengeluaranController@modal')->name('pengeluaran.modal')->middleware('top-level');

    //pemesanan
    Route::resource('pemesanan', 'PemesananController');
    Route::post('proses-pemesanan', 'PemesananController@process')->name('pemesanan.process');
    Route::get('show-pemesanan', 'PemesananController@showAll')->name('pemesanan.show_all');
    Route::get('show-pemesanan/{status_pemesanan}', 'PemesananController@showPemesanan');
    Route::put('konfirmasi-pemesanan/{pemesanan_id}/{status}', 'PemesananController@changeStatus')->name('pemesanan.changeStatus');
    Route::get('cancel-pemesanan/{pemesanan_id}', 'PemesananController@cancel')->name('pemesanan.cancel');
    //pembayaran
    Route::get('pembayaran/{pemesanan_id}', 'PembayaranController@index')->name('pembayaran');
    Route::post('pembayaran/store/{pemesanan_id}', 'PembayaranController@store')->name('pembayaran.store');

    //penjualan
    Route::middleware('top-level')->group(function(){
        Route::get('laporan/penjualan', 'LaporanController@penjualan')->name('laporan.penjualan');
        Route::get('laporan/pengeluaran', 'LaporanController@pengeluaran')->name('laporan.pengeluaran');
        Route::get('laporan/laba-rugi', 'LaporanController@labaRugi')->name('laporan.laba-rugi');
    });

    //ubah password
    Route::get('ubah-password', 'ChangePasswordController@index')->name('ubah_password');
    Route::post('ubah-password/change', 'ChangePasswordController@change')->name('ubah_password.change');

});

 //tracking pesanan
 Route::post('/search', function(Request $request){
    $request->validate([
        'kode' => 'required'
    ]);

    $pemesanan = Pemesanan::where('kode', $request->kode)->first();
    if(!$pemesanan){
        return redirect()->back()->with('error', 'Invoice / Kode tidak ditemukan');
    } else {

        return view('tracking', compact('pemesanan'));
    }

})->name('search-pesanan');